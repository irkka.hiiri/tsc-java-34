package ru.tsc.ichaplygina.taskmanager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class UserAlreadyLockedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "User already locked.";

    public UserAlreadyLockedException() {
        super(MESSAGE);
    }

}
