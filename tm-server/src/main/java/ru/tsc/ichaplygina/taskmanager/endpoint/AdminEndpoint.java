package ru.tsc.ichaplygina.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IDomainService;
import ru.tsc.ichaplygina.taskmanager.api.service.ISessionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.security.*;
import ru.tsc.ichaplygina.taskmanager.model.Session;
import ru.tsc.ichaplygina.taskmanager.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.UserUtil.getRole;

@WebService(name = "AdminEndpoint")
public class AdminEndpoint {

    @NotNull
    private final IAuthService authService;

    @NotNull
    private final IDomainService domainService;

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final IUserService userService;

    public AdminEndpoint(@NotNull final IUserService userService,
                         @NotNull final ISessionService sessionService,
                         @NotNull final IAuthService authService,
                         @NotNull final IDomainService domainService) {
        this.userService = userService;
        this.sessionService = sessionService;
        this.authService = authService;
        this.domainService = domainService;
    }

    @WebMethod
    public void addUser(@WebParam(name = "session") @Nullable final Session session,
                        @WebParam(name = "login") @NotNull final String login,
                        @WebParam(name = "password") @NotNull final String password,
                        @WebParam(name = "email") @NotNull final String email,
                        @WebParam(name = "role") @NotNull final String role,
                        @WebParam(name = "firstName") @Nullable final String firstName,
                        @WebParam(name = "middleName") @Nullable final String middleName,
                        @WebParam(name = "lastName") @Nullable final String lastName) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        userService.add(login, password, email, getRole(role), firstName, middleName, lastName);
    }

    @WebMethod
    public void changePassword(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "newPassword") @NotNull final String newPassword) {
        sessionService.validateSession(session);
        final boolean isAdmin = authService.isPrivilegedUser(session.getUserId());
        @NotNull final User user = Optional.ofNullable(userService.findById(session.getUserId()))
                .orElseThrow(UserNotFoundException::new);
        final boolean isCurrentLogin = user.getLogin().equals(login);
        if (isAdmin || isCurrentLogin) userService.setPassword(login, newPassword);
        else throw new AccessDeniedException();
    }

    @WebMethod
    public void changeRole(@WebParam(name = "session") @Nullable final Session session,
                           @WebParam(name = "login") @NotNull final String login,
                           @WebParam(name = "newRole") @NotNull final String newRole) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        userService.setRole(login, getRole(newRole));
    }

    @WebMethod
    public User findUserById(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "userId") @NotNull final String userId) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        return userService.findById(userId);
    }

    @WebMethod
    public User findUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "userLogin") @NotNull final String userLogin) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        return userService.findByLogin(userLogin);
    }

    @WebMethod
    public List<User> getUsers(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        return userService.findAll();
    }

    @WebMethod
    public void lockUserById(@WebParam(name = "session") @Nullable final Session session,
                             @WebParam(name = "userId") @NotNull final String userId) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        if (userId.equals(session.getUserId())) throw new UserSelfLockNotAllowedException();
        if (!userService.lockById(userId)) throw new UserAlreadyLockedException();
    }

    @WebMethod
    public void lockUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                @WebParam(name = "userLogin") @NotNull final String userLogin) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        @NotNull final User user = Optional.ofNullable(userService.findById(session.getUserId()))
                .orElseThrow(UserNotFoundException::new);
        final boolean isCurrentLogin = user.getLogin().equals(userLogin);
        if (isCurrentLogin) throw new UserSelfLockNotAllowedException();
        if (!userService.lockByLogin(userLogin)) throw new UserAlreadyLockedException();
    }

    @WebMethod
    public void removeUserById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "userId") @NotNull final String userId) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        if (userId.equals(session.getUserId())) throw new UserSelfDeleteNotAllowedException();
        Optional.ofNullable(userService.removeById(userId))
                .orElseThrow(UserNotFoundException::new);
    }

    @WebMethod
    public void removeUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "userLogin") @NotNull final String userLogin) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        @NotNull final User user = Optional.ofNullable(userService.findById(session.getUserId()))
                .orElseThrow(UserNotFoundException::new);
        final boolean isCurrentLogin = user.getLogin().equals(userLogin);
        if (isCurrentLogin) throw new UserSelfDeleteNotAllowedException();
        Optional.ofNullable(userService.removeByLogin(userLogin))
                .orElseThrow(UserNotFoundException::new);
    }

    @WebMethod
    public void unlockUserById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "userId") @NotNull final String userId) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        if (!userService.unlockById(userId)) throw new UserIsNotLockedException();
    }

    @WebMethod
    public void unlockUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "userLogin") @NotNull final String userLogin) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        if (!userService.unlockByLogin(userLogin)) throw new UserIsNotLockedException();
    }

    @WebMethod
    public void updateUserById(@WebParam(name = "session") @Nullable final Session session,
                               @WebParam(name = "userId") @NotNull final String userId,
                               @WebParam(name = "login") @NotNull final String login,
                               @WebParam(name = "password") @NotNull final String password,
                               @WebParam(name = "email") @NotNull final String email,
                               @WebParam(name = "role") @NotNull final String role,
                               @WebParam(name = "firstName") @Nullable final String firstName,
                               @WebParam(name = "middleName") @Nullable final String middleName,
                               @WebParam(name = "lastName") @Nullable final String lastName) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        Optional.ofNullable(userService.updateById(userId, login, password, email, getRole(role), firstName, middleName, lastName))
                .orElseThrow(UserNotFoundException::new);
    }

    @WebMethod
    public void updateUserByLogin(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "userLogin") @NotNull final String userLogin,
                                  @WebParam(name = "password") @NotNull final String password,
                                  @WebParam(name = "email") @NotNull final String email,
                                  @WebParam(name = "role") @NotNull final String role,
                                  @WebParam(name = "firstName") @Nullable final String firstName,
                                  @WebParam(name = "middleName") @Nullable final String middleName,
                                  @WebParam(name = "lastName") @Nullable final String lastName) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        Optional.ofNullable(userService.updateByLogin(userLogin, password, email, getRole(role), firstName, middleName, lastName))
                .orElseThrow(UserNotFoundException::new);
    }

    @WebMethod
    public void loadBackup(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadBackup();
    }

    @WebMethod
    public void loadBase64(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadBase64();
    }

    @WebMethod
    public void loadBinary(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadBinary();
    }

    @WebMethod
    public void loadJsonFasterXML(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadJsonFasterXML();
    }

    @WebMethod
    public void loadJsonJaxb(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadJsonJaxb();
    }

    @WebMethod
    public void saveBinary(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveBinary();
    }

    @WebMethod
    public void loadXMLFasterXML(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadXMLFasterXML();
    }

    @WebMethod
    public void loadXMLJaxb(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadXMLJaxb();
    }

    @WebMethod
    public void loadYAMLFasterXML(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.loadYAMLFasterXML();
    }

    @WebMethod
    public void saveBackup(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveBackup();
    }

    @WebMethod
    public void saveBase64(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveBase64();
    }

    @WebMethod
    public void saveJsonFasterXML(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveJsonFasterXML();
    }

    @WebMethod
    public void saveJsonJaxb(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveJsonJaxb();
    }

    @WebMethod
    public void saveXMLFasterXML(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveXMLFasterXML();
    }

    @WebMethod
    public void saveXMLJaxb(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveXMLJaxb();
    }

    @WebMethod
    public void saveYAMLFasterXML(@WebParam(name = "session") @Nullable final Session session) {
        sessionService.validateSession(session);
        sessionService.validatePrivileges(session.getUserId());
        domainService.saveYAMLFasterXML();
    }

}
