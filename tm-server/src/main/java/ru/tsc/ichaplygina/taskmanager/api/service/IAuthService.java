package ru.tsc.ichaplygina.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

public interface IAuthService {

    @Nullable String getCurrentUserId();

    void setCurrentUserId(@Nullable String currentUserId);

    @Nullable
    String getCurrentUserLogin();

    void checkRoles(@NotNull Role[] roles);

    boolean isNoUserLoggedIn();

    boolean isPrivilegedUser();

    boolean isPrivilegedUser(@NotNull String userId);

    void login(@NotNull String login, @NotNull String password);

    void logout();

    void throwExceptionIfNotAuthorized();

}
