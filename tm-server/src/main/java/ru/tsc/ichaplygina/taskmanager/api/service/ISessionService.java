package ru.tsc.ichaplygina.taskmanager.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.model.Session;

public interface ISessionService extends IService<Session> {

    Session openSession(@NotNull String login, @NotNull String password);

    void closeSession(@NotNull Session session);

    @SneakyThrows
    void validateSession(@NotNull Session session);

    @SneakyThrows
    void validatePrivileges(@NotNull String userId);
}
