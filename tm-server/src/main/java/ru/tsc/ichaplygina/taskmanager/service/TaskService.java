package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class TaskService extends AbstractBusinessEntityService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository repository;

    public TaskService(@NotNull final ITaskRepository repository, @NotNull final IAuthService authService) {
        super(repository, authService);
        this.repository = repository;
    }

    @Nullable
    @Override
    public final Task addTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (authService.isPrivilegedUser(userId)) return repository.addTaskToProject(taskId, projectId);
        return repository.addTaskToProjectForUser(userId, taskId, projectId);
    }

    @NotNull
    @Override
    public final List<Task> findAllByProjectId(@NotNull final String userId,
                                               @NotNull final String projectId,
                                               @Nullable final String sortBy) {
        @NotNull final Comparator<Task> comparator = getComparator(sortBy);
        if (authService.isPrivilegedUser(userId)) return repository.findAllByProjectId(projectId, comparator);
        return repository.findAllByProjectIdForUser(userId, projectId, comparator);
    }

    @Override
    public final void removeAllByProjectId(@NotNull final String projectId) {
        repository.removeAllByProjectId(projectId);
    }

    @Nullable
    @Override
    public final Task removeTaskFromProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        if (authService.isPrivilegedUser(userId)) return repository.removeTaskFromProject(taskId, projectId);
        return repository.removeTaskFromProjectForUser(userId, taskId, projectId);
    }

}
